from flask import Flask, render_template, request, session, redirect, url_for, send_from_directory
from flask_session import Session

import os
import yt_dlp

app = Flask(__name__)
app.config["SESSION_PERMANENT"] = True
app.config["SESSION_TYPE"] = "filesystem"
Session(app)


USERNAME = "username"
PASSWORD = "password"
PATH = "/temp"

cache = {}


########################################################
#                                                      #
#  Home Page                                           #
#  /                                                   #
#                                                      #
########################################################
@app.route('/', methods=['GET','POST'])
def home():
    if not session.get("username"):
        return redirect(url_for('login'))
    else:
        return redirect(url_for('download'))
        

########################################################
#                                                      #
#  Login Page                                          #
#  /login                                              #
#                                                      #
########################################################
@app.route('/login', methods=['GET','POST'])
def login():
    if session.get("username"):
        return redirect(url_for('download'))

    error = None
    if request.method == 'POST':
        if request.form['username'] != USERNAME or request.form['password'] != PASSWORD:
            error = "Invalid credentials. Please try again."
        else:
            # Creating a session to stay logged in.
            session["username"] = request.form.get("username")

            return redirect(url_for('download'))

    return render_template('login.html', error=error)


########################################################
#                                                      #
#  Logout Page                                         #
#  /logout                                             #
#                                                      #
########################################################
@app.route('/logout', methods=['GET','POST'])
def logout():
    if session.get("username"):
        session.clear()

    return redirect(url_for('login'))


########################################################
#                                                      #
#  Download Page                                       #
#  /download                                           #
#                                                      #
########################################################
@app.route('/download', methods=['GET', 'POST'])
def download():
    if not session.get("username"):
        return redirect(url_for('login'))

    return render_template('download.html')


########################################################
#                                                      #
#  Video Page                                          #
#  /video                                              #
#                                                      #
########################################################
@app.route('/video', methods=['GET', 'POST'])
def video():
    if not session.get("username"):
        return redirect(url_for('login'))

    if request.method == 'POST':
        link = request.form.get('videolink', "")
        
        path = f"{os.getcwd()}{PATH}"

        ydl_opts = {
            'outtmpl': os.path.join(path, '%(id)s.mp4'),  # name the file the ID of the video
            'format': 'bestvideo[ext=mp4]/best',          # choice of quality
            'audioformat': 'mp4',                         # convert to mp4
            'noplaylist': True                            # only download single song, not playlist
        }

        with yt_dlp.YoutubeDL(ydl_opts) as ydl:
            info_dict = ydl.extract_info(link, download=False)
            id = info_dict["id"]
            title = info_dict.get('title', 'Undefined')
            author = info_dict.get('artist', 'Undefined')

            if author == "Undefined" and " - " in title:
                author = title.split(" - ")[0]
                title = title.split(" - ")[1]

            thumbnail = info_dict.get('thumbnail', '')
            ydl.download([link])

        return render_template('status.html', image=thumbnail, url=link, title=title, rawTitle=title.replace(".", " "), author=author, path=f"/temp/{id}.mp4")

    return redirect(url_for('download'))


########################################################
#                                                      #
#  Temp Page                                           #
#  /temp                                               #
#                                                      #
########################################################
@app.route('/temp/<path:path>', methods=['GET', 'POST'])
def temp(path):
    try:
        return send_from_directory(f"{os.getcwd()}{PATH}", path)
    except:
        return redirect(url_for('download'))


app.run(host="0.0.0.0", port=80, debug=False)