# Utilisation

## Docker (recommandé)
```sh
docker compose up -d
```

## Manuel
```sh
pip install -r requirements.txt
python application.py
```